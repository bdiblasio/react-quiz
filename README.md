# THIS IS A SIMPLE QUIZ APP
###### there are many more like it but this one is mine
![Alt Text](https://media.giphy.com/media/3ndAvMC5LFPNMCzq7m/giphy-downsized-large.gif)
------------
------------
**Made with React, using [useState, useEffect, and useRef] hooks**\
Styled with TailwindCSS
*Data populated from custom JSON.*
