import React from "react";
import "../input.css";

const Start = ({ onQuizStart }) => {
  return (
    <div className="h-screen flex justify-center items-center bg-gradient-to-b from-black via-indigo-900 to-purple-900">
      <div className="flex flex-col items-center w-4/6 h-5/6 p-4 border-4 border-cyan-500 border-double rounded-xl bg-white">
        <h1 className="font-Caprasimo text-8xl uppercase hover:font-mono mt-4">
          ars longa
        </h1>

        {/* this puts children in the center */}
        <div className="flex-grow"></div>
        <div className="flex flex-col items-center w-full"></div>


        <img
          className="rounded-xl w-max"
          src="https://news.artnet.com/app/news-upload/2014/09/gif-art-history-wallpapers.gif"
          alt="Art History"
        />
        <p className="font-mono text-2xl mt-4">unlock the canvas of time</p>
        <p className="font-mono text-2xl">test your art history knowledge</p>
        <button
          className="border-2 border-double rounded-xl w-24 border-pink-400 hover:bg-sky-200 hover:w-48 mt-4"
          onClick={onQuizStart}
        >
          {">>>"}
        </button>

        {/* this puts children at the bottom */}
        <div className="flex-grow"></div>
        <div className="flex flex-col items-center w-full"></div>
        <h1 className="font-Caprasimo text-7xl uppercase hover:font-mono mb-4">
          vita brevis
        </h1>
        
      </div>
    </div>
  );
};

export default Start;
