import React from "react";
import '../input.css';

const Modal = ({onClose, results, data}) => {
    return (
        <div className="modal-wrapper">
            <div className="modal-background" onClick={onClose}> </div>
            <div className="modal-card">
                <header className="modal-card-head">
                    <p className='modal-card-title'> Your Answers</p>
                    <button className="delete" onClick={onClose}>Close </button>
                </header>
                <section className="modal-card-body">
                    <ul>
                        {results.map((result, i) => (
                            <li key={i}>
                            <p><strong>{result.q}</strong></p>
                            <p className={result.a === data[i].answer ? 'green' :
                            'red'}> Your Answer: {result.a}</p>
                            {result.a !== data[i].answer && <p className="blue"> Correct Answer:
                            {data[i].answer} </p>}
                            </li>
                        ))}
                    </ul>
                </section>

            </div>
        </div>
    )
}

export default Modal;
