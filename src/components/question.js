import React, { useState, useEffect, useRef } from "react";

const Question = ({
  data,
  onAnswerUpdate,
  numberOfQuestions,
  activeQuestion,
  onSetActiveQuestion,
  onSetStep,
}) => {
  const [selected, setSelected] = useState("");
  const [error, setError] = useState("");
  const radiosWrapper = useRef();

  useEffect(() => {
    const findCheckedInput =
      radiosWrapper.current.querySelector("input:checked");
    if (findCheckedInput) {
      findCheckedInput.checked = false;
    }
  }, [data]);

  const changeHandler = (e) => {
    setSelected(e.target.value);
    if (error) {
      setError("");
    }
  };

  const nextClickHandler = (e) => {
    if (selected === "") {
      return setError("Please Select One Option!");
    }
    onAnswerUpdate((prevState) => [
      ...prevState,
      { q: data.question, a: selected },
    ]);
    setSelected("");
    if (activeQuestion < numberOfQuestions - 1) {
      onSetActiveQuestion(activeQuestion + 1);
    } else {
      onSetStep(3);
    }
  };

  return (
    // background container
    <div className="h-screen flex justify-center items-center bg-gradient-to-b from-black via-indigo-900 to-purple-900">

        {/* card container */}
      <div className="flex items-center w-4/6 h-5/6 p-4 border-4 border-cyan-500 border-double rounded-xl bg-neutral-50">


        {/* question and answer container */}
        <div className="content border-2 border-red-200 justify-center items-center">


            {/* question container */}
          <h2 className="text-center font-mono text-5xl border-2 border-green-300 uppercase hover:font-mono mt-4">
            {data.question}
          </h2>


          {/* answer container */}
          <div className="border-2 border-blue-500" ref={radiosWrapper}>

            {/* choices container */}
            {data.choices.map((choice, i) => (
              <label className="flex text-center items-center rounded-md border-2 m-5 p-2 border-green-900 hover:bg-sky-200" key={i}>
                <input
                  type="radio"
                  className=""
                  value={choice}
                  onChange={changeHandler}
                />
                {choice}
              </label>
            ))}
          </div>

          {/* error message */}
          {error && <div className="ml-5 font-mono uppercase"> {error} </div>}


        {/* submit/next button */}
          <button
            className="ml-5 mb-10 border-2 border-double rounded-xl w-24 border-pink-400 hover:bg-sky-200 mt-6"
            onClick={nextClickHandler}
          >next</button>


        </div>
      </div>
    </div>
  );
};

export default Question;
