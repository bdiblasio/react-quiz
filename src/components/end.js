import React, { useState, useEffect } from "react";
import {formatTime} from '../utils';
import '../input.css';

const End = ({results, data, onReset, onAnswersCheck, time}) => {
    const [correctAnswers, setCorrectAnswers] = useState(0);

    useEffect(() => {
        let correct = 0;
        results.forEach((result, index) => {
            if(result.a === data[index].answer) {
                correct++;
            }
        });
        setCorrectAnswers(correct);
    }, []);


    return (
        <div className="card">
            <div className="card-content">
                <div className="content">
                    <h3>your results </h3>
                        <p> {correctAnswers} of {data.length} </p>
                        <p> {Math.floor(correctAnswers / data.length) * 100}%</p>
                        <p><strong>your time: </strong> {formatTime(time)} </p>
                        <button className="button-answers" onClick={onAnswersCheck}> check your answers</button>
                        <button className="button-retry" onClick={onReset}> try again</button>
                </div>
            </div>
        </div>
    )
}

export default End;
