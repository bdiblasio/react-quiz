/** @type {import('tailwindcss').Config} */

module.exports = {
  content: [
    "./src/**/*.html",
    "./src/**/*.js",
    "./app.js/"
  ],
  theme: {
    extend: {
      fontFamily: {
        'Caprasimo' : ['Caprasimo', 'serif']
      }
    },
  },
  plugins: [],
};
